module example_test
  use vegetables, only: test_item_t, result_t, describe, it, succeed

  implicit none
  private
  public :: test_example
contains
  function test_example() result(tests)
    type(test_item_t) :: tests

    tests = describe("the test", [it("succeed", check_succeeds)])
  end function

  function check_succeeds() result(result_)
    type(result_t) :: result_

    result_ = succeed("successfully")
  end function
end module
